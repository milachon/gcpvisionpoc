package com.milachon.gcpvisionpoc;

import java.awt.Color;
import org.springframework.test.context.ActiveProfiles;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;

@ActiveProfiles("test")
public class AbstractTest {

  protected CatalogItem item1 = new CatalogItem();
  protected CatalogItem item2 = new CatalogItem();
  protected CatalogItem item3 = new CatalogItem();

  protected Color c1 = new Color(15, 30, 55);
  protected Color c2 = new Color(150, 230, 155);
  protected Color c3 = new Color(150, 30, 55);

}

package com.milachon.gcpvisionpoc.service;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.google.type.Color;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageAnalysisServiceTest {

  @Autowired
  protected ImageAnalysisService service;

  @Test
  public void testGetDominantColor() {

    Color result = service.getDominantColor(
        "https://image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/default/DH7983_1ZH_24.jpg?sw=458&sh=443");

    assertEquals(108.0, result.getBlue(), 0.0);
    assertEquals(45.0, result.getGreen(), 0.0);
    assertEquals(29.0, result.getRed(), 0.0);
  }

  @Test
  public void testGetDominantColorFileDoesntExist() {

    Color result = service.getDominantColor(
        "https://image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/default/PH3468_001_24.jpg?sw=458&sh=443");
    assertEquals(null, result);
  }

}

package com.milachon.gcpvisionpoc.api;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.milachon.gcpvisionpoc.AbstractTest;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;
import com.milachon.gcpvisionpoc.manager.CatalogItemManager;

@RunWith(SpringRunner.class)
@WebMvcTest(CatalogItemRestController.class)
public class CatalogItemRestControllerTest extends AbstractTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CatalogItemManager manager;

  @Test
  public void getItemByDominantColorDistanceTest() throws Exception {

    List<CatalogItem> res = new ArrayList<CatalogItem>();

    item2.setCurrentDominantColorDist(403.0);
    item2.setItemId("id2");
    item3.setCurrentDominantColorDist(202.0);
    item3.setItemId("id3");

    res.add(item2);
    res.add(item3);

    when(manager.computeDistDominantColorForAllCatalogue("id1")).thenReturn(res);
    this.mockMvc.perform(get("/catalog_items/id1/nearestItems")//
        .param("nbMax", "1"))//
        .andExpect(status().isOk())//
        .andExpect(content().string(containsString(item3.getItemId()))); // TODO check that is item3
                                                                         // only
  }

}

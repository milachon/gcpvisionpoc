package com.milachon.gcpvisionpoc.manager;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.milachon.gcpvisionpoc.AbstractTest;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CatalogItemManagerTest extends AbstractTest {

  @Autowired
  private CatalogItemManager manager;

  @Before
  public void initDb() {

    item1.setDominantColor(c1);
    item1.setItemId("id1");
    manager.save(item1);

    item2.setDominantColor(c2);
    item2.setItemId("id2");
    manager.save(item2);

    item3.setDominantColor(c3);
    item3.setItemId("id3");
    manager.save(item3);
  }

  @Test
  public void testComputeDistDominantColorForAllCatalogue() {
    List<CatalogItem> result = manager.computeDistDominantColorForAllCatalogue("id1");

    assertEquals(2, result.size());

    assertEquals("id2", result.get(0).getItemId());
    assertEquals(478.5854155738555, result.get(0).getCurrentDominantColorDist(), 0.0000001);
    assertEquals("id3", result.get(1).getItemId());
    assertEquals(205.63803150195733, result.get(1).getCurrentDominantColorDist(), 0.0000001);
  }

}

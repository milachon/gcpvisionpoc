package com.milachon.gcpvisionpoc.utils;

import static org.junit.Assert.assertEquals;
import java.io.FileNotFoundException;
import java.util.List;
import org.junit.Test;
import com.milachon.gcpvisionpoc.domain.enumerator.Gender;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;

public class CSVUtilsTest {

  @Test
  public void testReadCatalogItemFromCSVFile() throws IllegalStateException, FileNotFoundException {
    List<CatalogItem> result =
        CSVUtils.readCatalogItemFromCSVFile("./src/test/resources/products-test.csv", ';');

    assertEquals(499, result.size());
    assertEquals("PH3468-00-001", result.get(19).getItemId());
    assertEquals(Gender.MAN, result.get(19).getGender());
  }

}

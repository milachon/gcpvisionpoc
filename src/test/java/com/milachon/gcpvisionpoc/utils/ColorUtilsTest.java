package com.milachon.gcpvisionpoc.utils;

import static org.junit.Assert.assertEquals;
import java.awt.Color;
import org.junit.Test;

public class ColorUtilsTest {

  @Test
  public void testRGBDistance() {

    Color c1 = new Color(15, 30, 55);
    Color c2 = new Color(15, 30, 55);
    Color c3 = new Color(150, 3, 42);

    assertEquals(0.0, ColorUtils.RGBDistance(c1, c2), 0.0);
    assertEquals(213.67030678126523, ColorUtils.RGBDistance(c1, c3), 0.00000001);
  }

}

package com.milachon.gcpvisionpoc;

import java.io.FileNotFoundException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.milachon.gcpvisionpoc.manager.CatalogItemManager;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class App {

  public static void main(String[] args) throws IllegalStateException, FileNotFoundException {

    if (args.length > 0) {


      // select mode
      switch (args[0]) {
        case "db_update":
          if (args.length > 1) {
            // create app
            ApplicationContext ctx = new SpringApplication(App.class).run(args);

            ctx.getBean(CatalogItemManager.class).dbUpdateFromFile(args[1]);

            // close app
            SpringApplication.exit(ctx);
          } else {
            System.err.println("Missing catalog file path.\n");
          }
          break;
        case "image_analysis":
          // create app
          ApplicationContext ctx = new SpringApplication(App.class).run(args);

          ctx.getBean(CatalogItemManager.class).imageAnalysisAll();

          // close app
          SpringApplication.exit(ctx);
          break;
        case "api":
          new SpringApplication(App.class).run(args);
          break;
        default:
          System.err.println("Unkown mode (db_update or image_analysis or api).\\n");
          break;
      }
    } else {
      System.err.println("Missing mode (db_update or image_analysis or api).\n");
    }

  }

}

package com.milachon.gcpvisionpoc.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface IAbstractRepository<T> extends PagingAndSortingRepository<T, String> {

}

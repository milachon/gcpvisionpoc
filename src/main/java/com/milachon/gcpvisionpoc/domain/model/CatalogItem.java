package com.milachon.gcpvisionpoc.domain.model;

import java.awt.Color;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import com.milachon.gcpvisionpoc.domain.enumerator.Gender;
import com.milachon.gcpvisionpoc.utils.ColorDBConverter;
import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "catalog_item")
@Getter
@Setter
public class CatalogItem extends AbstractEntity {

  @Column
  @CsvBindByName(column = "id")
  private String itemId;

  @Column
  @CsvBindByName(column = "title")
  private String title;

  @Enumerated(EnumType.STRING)
  @Column
  @CsvBindByName(column = "gender_id")
  private Gender gender;

  @Column
  @CsvBindByName(column = "composition")
  private String composition;

  @Column
  @CsvBindByName(column = "sleeve")
  private String sleeve;

  @Column
  @CsvBindByName(column = "photo")
  private String photo;

  @Column
  @CsvBindByName(column = "url")
  private String url;

  @Column
  @Convert(converter = ColorDBConverter.class)
  private Color dominantColor;

  private Double currentDominantColorDist;

}


package com.milachon.gcpvisionpoc.domain.enumerator;

/** Canal used to send messages **/
public enum Gender {
  MAN, WOM, GIR, BOY, UNI;
}

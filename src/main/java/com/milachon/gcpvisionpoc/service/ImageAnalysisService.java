package com.milachon.gcpvisionpoc.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.vision.CloudVisionException;
import org.springframework.cloud.gcp.vision.CloudVisionTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.ColorInfo;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.type.Color;

@Service
public class ImageAnalysisService {

  @Autowired
  private CloudVisionTemplate cloudVisionTemplate;

  @Autowired
  private ResourceLoader resourceLoader;

  public Color getDominantColor(String url) {

    Resource imageResource = resourceLoader.getResource(url);
    
    AnnotateImageResponse imgMetaData;
    try {
      imgMetaData = cloudVisionTemplate.analyzeImage(imageResource, Type.IMAGE_PROPERTIES);
    } catch (CloudVisionException e) {
      System.err.println("An error occured with file " + url + "\n");
      return null;
    }


    // need to use a copy cause result is Unmodifiable
    List<ColorInfo> colors = new ArrayList<>(
        imgMetaData.getImagePropertiesAnnotation().getDominantColors().getColorsList());

    if (colors.size() == 0) {
      return null;
    }

    // sort by descending
    Collections.sort(colors, new Comparator<ColorInfo>() {
      @Override
      public int compare(ColorInfo c, ColorInfo c1) {
        // Choose to compare on score seems better than on pixelfraction
        return Float.valueOf(c1.getScore()).compareTo(c.getScore());
      }
    });

    return colors.get(0).getColor();

  }

}

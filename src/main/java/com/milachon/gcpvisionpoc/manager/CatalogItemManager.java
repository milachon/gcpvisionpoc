package com.milachon.gcpvisionpoc.manager;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.type.Color;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;
import com.milachon.gcpvisionpoc.repository.ICatalogItemRepository;
import com.milachon.gcpvisionpoc.service.ImageAnalysisService;
import com.milachon.gcpvisionpoc.utils.CSVUtils;
import com.milachon.gcpvisionpoc.utils.ColorDBConverter;
import com.milachon.gcpvisionpoc.utils.ColorUtils;

@Service
public class CatalogItemManager extends AbstractManager<ICatalogItemRepository, CatalogItem> {

  @Autowired
  public ImageAnalysisService imgAnalysisService;

  public void imageAnalysisAll() {
    // Read all entry
    List<CatalogItem> catalog = getAll();

    // Get dominant color for "new" item
    // FIXME directly get only item with dominant null from db to optimize
    for (CatalogItem item : catalog) {
      if (item.getDominantColor() == null) {
        // FIXME in catalog url are without https and it doesn't work here
        String url = "https:" + item.getPhoto();
        Color domColor = imgAnalysisService.getDominantColor(url);

        if (domColor == null) {
          continue;
        }

        // need to swap type from google to java awt
        item.setDominantColor(ColorUtils.fromProto(domColor));

        System.out.println("Image from " + url + " has been analysed. Dominant color is : "
            + new ColorDBConverter().convertToDatabaseColumn(item.getDominantColor()) + "\n");

        // Persist in DB
        save(item);
      }
    }
  }

  public void dbUpdateFromFile(String catalogPath)
      throws IllegalStateException, FileNotFoundException {
    List<CatalogItem> datas = CSVUtils.readCatalogItemFromCSVFile(catalogPath, ';');

    // TODO instead of removing everything, develop an update method to avoid loosing external api
    // result
    removeAll();
    saveList(datas);
  }

  public CatalogItem getById(String itemId) {
    return repo.findByItemId(itemId).orElseGet(null);
  }

  // TODO persist the result maybe ? to avoid computing each time
  public List<CatalogItem> computeDistDominantColorForAllCatalogue(String id) {

    List<CatalogItem> result = new ArrayList<>();
    CatalogItem refItem = getById(id);

    // compute distance for all item
    for (CatalogItem item : getAll()) {
      // except itself
      if (!item.getItemId().equals(refItem.getItemId())) {
        Double dist = ColorUtils.RGBDistance(refItem.getDominantColor(), item.getDominantColor());
        if (dist != null) {
          item.setCurrentDominantColorDist(dist);
          result.add(item);
        }
      }
    }

    return result;
  }
}

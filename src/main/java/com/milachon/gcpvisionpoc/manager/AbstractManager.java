package com.milachon.gcpvisionpoc.manager;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.milachon.gcpvisionpoc.domain.model.AbstractEntity;
import com.milachon.gcpvisionpoc.repository.IAbstractRepository;

public abstract class AbstractManager<R extends IAbstractRepository<E>, E extends AbstractEntity>
    implements ICRUDManager<E> {

  @Autowired
  protected R repo;

  @Override
  public E save(E entity) {
    return repo.save(entity);
  }

  @Override
  public void saveList(List<E> entities) {
    repo.saveAll(entities);
  }

  @Override
  public void removeAll() {
    repo.deleteAll();
  }

  @Override
  public List<E> getAll() {
    // convert iterable to array list
    List<E> list = new ArrayList<>();
    repo.findAll().forEach(list::add);

    return list;
  }


}

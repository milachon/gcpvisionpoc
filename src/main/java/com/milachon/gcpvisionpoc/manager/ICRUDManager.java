package com.milachon.gcpvisionpoc.manager;

import java.util.List;

public interface ICRUDManager<E> {

  public E save(E entity);

  public void saveList(List<E> entities);

  public void removeAll();

  public List<E> getAll();

}

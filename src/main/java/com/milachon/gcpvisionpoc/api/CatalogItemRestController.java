package com.milachon.gcpvisionpoc.api;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.Min;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;
import com.milachon.gcpvisionpoc.manager.CatalogItemManager;;

@RestController
@RequestMapping("/catalog_items")
public class CatalogItemRestController
    extends AbstractRestController<CatalogItemManager, CatalogItem> {

  @GetMapping(value = "/{itemId}/nearestItems")
  public ResponseEntity<List<CatalogItem>> getItemByDominantColorDistance(//
      @PathVariable String itemId, //
      @RequestParam(required = false) @Min(0) Integer nbMax) {

    List<CatalogItem> result = manager.computeDistDominantColorForAllCatalogue(itemId);

    List<CatalogItem> sortedFilteredResult = result.stream()//
        .sorted(Comparator.comparingDouble(CatalogItem::getCurrentDominantColorDist))//
        .limit(nbMax != null ? nbMax : Integer.MAX_VALUE)//
        .collect(Collectors.toList());

    return new ResponseEntity<>(sortedFilteredResult, HttpStatus.OK);

  }

}

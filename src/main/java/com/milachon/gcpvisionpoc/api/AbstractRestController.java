package com.milachon.gcpvisionpoc.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.milachon.gcpvisionpoc.manager.ICRUDManager;

@RestController
public abstract class AbstractRestController<T extends ICRUDManager<E>, E> {

  @Autowired
  protected T manager;

  @GetMapping(value = "")
  public List<E> getList() {
    return manager.getAll();
  }



}

package com.milachon.gcpvisionpoc.utils;

import java.awt.Color;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ColorDBConverter implements AttributeConverter<Color, String> {

  private static final String SEPARATOR = ",";

  /**
   * Convert Color object to a String with format red|green|blue|alpha
   */
  @Override
  public String convertToDatabaseColumn(Color color) {
    if (color == null) {
      return null;
    }
    StringBuilder sb = new StringBuilder();
    sb.append(color.getRed()).append(SEPARATOR).append(color.getGreen()).append(SEPARATOR)
        .append(color.getBlue());
    return sb.toString();
  }

  /**
   * Convert a String with format red|green|blue|alpha to a Color object
   */
  @Override
  public Color convertToEntityAttribute(String colorString) {
    if (colorString == null || colorString.isEmpty()) {
      return null;
    }
    String[] rgb = colorString.split(SEPARATOR);

    return new Color(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]));
  }

}

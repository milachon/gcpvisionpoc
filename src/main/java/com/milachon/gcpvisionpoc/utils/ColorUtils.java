package com.milachon.gcpvisionpoc.utils;

import java.awt.Color;

public class ColorUtils {

  public static Color fromProto(com.google.type.Color protocolor) {
    return new Color(Math.round(protocolor.getRed()), Math.round(protocolor.getGreen()),
        Math.round(protocolor.getBlue()));
  }

  public static Double RGBDistance(Color c1, Color c2) {

    if (c1 == null || c2 == null) {
      return null;
    }

    // formula issue from https://www.compuphase.com/cmetric.htm

    long rMean = (c1.getRed() + c2.getRed()) / 2;
    long r = c1.getRed() - c2.getRed();
    long g = c1.getGreen() - c2.getGreen();
    long b = c1.getBlue() - c2.getBlue();
    return Math.sqrt((((512 + rMean) * r * r) >> 8) + 4 * g * g + (((767 - rMean) * b * b) >> 8));

  }
}

package com.milachon.gcpvisionpoc.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import com.milachon.gcpvisionpoc.domain.model.CatalogItem;
import com.opencsv.bean.CsvToBeanBuilder;

public class CSVUtils {

  public static List<CatalogItem> readCatalogItemFromCSVFile(String filePath, char separator)
      throws IllegalStateException, FileNotFoundException {

    return new CsvToBeanBuilder<CatalogItem>(new FileReader(filePath)) //
        .withType(CatalogItem.class) //
        .withSeparator(separator) //
        .build()//
        .parse();
  }

}

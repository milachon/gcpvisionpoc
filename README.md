# GCPVisionPoc
[![pipeline status](https://gitlab.com/milachon/GCPVisionPoc/badges/master/pipeline.svg)](https://gitlab.com/milachon/GCPVisionPoc/-/commits/develop)


## Prerequisites

* Need Java >8
* a gcp project project with vision api and billing activated (cf https://cloud.google.com/vision/docs/quickstart-client-libraries)

## Build
* execute command "./gradlew assemble" or "gradlew.bat assemble" with Windows
* jar is available in /build/libs/
* jar is also available (limited time) in artifact of CI job
* Various quality/coverage/... reports are available in ./builds/reports

## Test
* Log your gcloud account (Best way is to define GOOGLE_APPLICATION_CREDENTIALS with a key json file (see https://cloud.google.com/docs/authentication/getting-started )
* execute command "./gradlew test" or "gradlew.bat test" with Windows

## Launch catalog reading and persist
* Execute command: java -jar GcpVisionPoc-0.1.0.jar db_update $csv_file_path$

* Several example files are in .\src\test\resources 

## Launch Dominant color analysis and persist
* Log your gcloud account (Best way is to define GOOGLE_APPLICATION_CREDENTIALS with a key json file (see https://cloud.google.com/docs/authentication/getting-started )
* Execute command: java -jar GcpVisionPoc-0.1.0.jar image_analysis

## Launch api server
* Execute command: java -jar GcpVisionPoc-0.1.0.jar api [--server.port=8080]

## Exposed API 
Auto generated api doc: http://localhost:8080/swagger-ui.html


## Admin: Config 
Configuration (like DB connection) are in src/main/resources/application.yml


